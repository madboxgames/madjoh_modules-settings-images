define(function (){
	var Images = {
		// GENERIC
			JUSTDARE_LOGO 			: 'app/images/Generic/justdare_logo.svg',
			JUSTDARE_ICON_WHITE 	: 'app/images/Generic/justdare_icon_white.svg',
			JUSTDARE_ICON_ORANGE 	: 'app/images/Generic/justdare_icon_orange.svg',
			JUSTDARE_ICON_BROKEN 	: 'app/images/Generic/justdare_icon_broken.svg',
			PROFILE_PICTURE 		: 'app/images/Generic/profile_picture.svg',
			FACEBOOK 				: 'app/images/Generic/facebook.svg',
			PLUS 					: 'app/images/Generic/plus.svg',
			MINUS 					: 'app/images/Generic/minus.svg',
			CLOSE 					: 'app/images/Generic/close.svg',
			SEARCH 					: 'app/images/Generic/search.svg',
			STAR 					: 'app/images/Generic/star.svg',
			COIN 					: 'app/images/Generic/coin.svg',
			YES 					: 'app/images/Generic/yes.svg',
			NO 						: 'app/images/Generic/no.svg',

		// LOADING
			SPINNER 				: 'app/images/Loading/spinner.svg',

		// MEDIA SELECTOR
			MEDIA_PHOTO 			: 'app/images/Generic/justdare_icon_orange.svg',
			MEDIA_VIDEO 			: 'app/images/Generic/justdare_icon_orange.svg',
			MEDIA_GALLERY 			: 'app/images/Generic/justdare_icon_orange.svg',

		// TUTORIAL
			TUTO_WORLD 				: 'app/images/Tutorial/world.svg',

		// FOOTER
			FOOTER_CHALLENGES 		: 'app/images/Footer/challenges.svg',
			FOOTER_ACTUALITIES 		: 'app/images/Footer/actualities.svg',
			FOOTER_FRIENDS 			: 'app/images/Footer/friends.svg',
			FOOTER_NOTIFICATIONS 	: 'app/images/Footer/notifications.svg',
			FOOTER_MENU 			: 'app/images/Footer/menu.svg',

		// CHALLENGES
			CHALLENGE_CREATE 		: 'app/images/Challenges/create.svg',
			CHALLENGE_SENT 			: 'app/images/Challenges/sent.svg',
			CHALLENGE_RECEIVED 		: 'app/images/Challenges/received.svg',

		// ACTUALITIES
			TOP_DARES 				: 'app/images/Actualities/top_dares.svg',
			QUICK_DARE 				: 'app/images/Actualities/quick_dare.svg',
			LIKE 					: 'app/images/Actualities/like.svg',
			COMMENT 				: 'app/images/Actualities/comment.svg',
			RESEND 					: 'app/images/Actualities/resend.svg',

		// FRIENDSHIP
			FRIENDSHIP_SUGGEST 		: 'app/images/Friendship/suggest.svg',
			FRIENDSHIP_PENDING 		: 'app/images/Friendship/pending.svg',
			FRIENDSHIP_FRIENDS 		: 'app/images/Friendship/friends.svg',
			FRIENDSHIP_IDOLES 		: 'app/images/Friendship/idoles.svg',
			FRIENDSHIP_FOLLOWERS 	: 'app/images/Friendship/followers.svg',

		// MENU
			MENU_FRIENDS 			: 'app/images/Menu/friends.svg',
			MENU_IDOLES 			: 'app/images/Menu/idoles.svg',
			MENU_FOLLOWERS 			: 'app/images/Menu/followers.svg',
			MENU_ACCOUNT 			: 'app/images/Menu/account.svg',
			MENU_STARS 				: 'app/images/Menu/stars.svg',
			MENU_LEGAL 				: 'app/images/Menu/legal.svg',
			MENU_WEBSITE 			: 'app/images/Menu/website.svg',
			MENU_RATE 				: 'app/images/Menu/rate.svg',
			MENU_LOGOUT 			: 'app/images/Menu/logout.svg',

		// NETWORKS
			NETWORK_FACEBOOK 		: 'app/images/Networks/facebook.svg',
			NETWORK_GOOGLE 			: 'app/images/Networks/google.svg',
			NETWORK_INSTAGRAM 		: 'app/images/Networks/instagram.svg',
			NETWORK_TWITTER 		: 'app/images/Networks/twitter.svg',

		// USER
			USER_MALE 				: 'app/images/User/male.svg',
			USER_FEMALE 			: 'app/images/User/female.svg',

		// TRANSACTIONS
			FACEBOOK 				: 'app/images/Transactions/facebook.svg',
			GAME_GIFT 				: 'app/images/Transactions/game_gift.svg',
			GAME_REQUEST 			: 'app/images/Transactions/game_request.svg',
			INVITE 					: 'app/images/Transactions/invite.svg',
			UPDATE_ACCOUNT 			: 'app/images/Transactions/update_account.svg',
			VISIT_MADJOH 			: 'app/images/Transactions/visit_madjoh.svg',
			WATCH_AD 				: 'app/images/Transactions/watch_ad.svg'
	};

	return Images;
});